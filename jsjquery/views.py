from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import EntryForm
from .models import Status
from django.utils import timezone
import datetime

# Create your views here.
def input_status(request):
    inputan = EntryForm()
    status = Status.objects.all().order_by("-id")
    if request.method=="POST":
        inputan = EntryForm(request.POST)
        if inputan.is_valid():
            db = Status()
            db.input_status = inputan.cleaned_data["input_status"]
            db.save()
        return redirect('/')
    # response = {
    #     'input_status' : status,
    #     'server_time' : datetime.datetime.now(),
    # }
    return render(request, "landing.html", {'EntryForm':inputan, 'Status':status})
