from django.contrib import admin
from django.urls import path
from . import views

app_name='jsjquery'
urlpatterns = [
    path('', views.input_status, name="jsjquery"),
]
